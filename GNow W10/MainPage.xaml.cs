﻿using GNow_W10.Model.Generic;
using GNow_W10.Pages;
using Newtonsoft.Json;
using Stop_Diabetes.Supernova.Messages;
using Supernova.Entities;
using Supernova.GNow;
using Supernova.Model.Database;
using Supernova.Model.Database.dao;
using Supernova.Model.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.System.Profile;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.StartScreen;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace GNow_W10
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        MessageBox msgBox;
        private readonly LoadTranslations translations;
        public const string appbarTileId = "MySecondaryTileGNow";
        private Supernova.Entities.Profile profile;
        private readonly Gravity G;
        private bool Started = false;
        readonly GravityPlanets data;
        public MainPage()
        {
            this.InitializeComponent();
            G = new Gravity();
            translations = new LoadTranslations();
            data = new GravityPlanets();

            try
            {
                var status = Supernova.Core.InAppPurchases.Check_Status("DonationGNow");

                if (status)
                {
                    btnDonation.Visibility = Visibility.Collapsed;
                    gridAds.Visibility = Visibility.Collapsed;
                }
            }
            catch { }
        }
        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            await GetProfile();
            GetSinglePositionAsync();
            ChangePinInfo();
            Started = true;
            lblDescription1.Text = data.GetDescription(3);
        }
        public async void GetSinglePositionAsync()
        {
            if (!profile.sync)
            {
                lblUnitsG.Visibility = Visibility.Visible;
                msgBox = new MessageBox();
                Clean();
                try
                {
                    double gravityR = 0;
                    InfoLocation iLoc = await GetLocation();

                    lblAltitude.Text = iLoc.altitude.ToString();
                    lblLatitude.Text = iLoc.latitude.ToString();
                    lblLongitude.Text = iLoc.longitude.ToString();

                    if (profile.units == Units.Meters)
                        lblAltitudeS.Text = $"{translations.Get_Translation("lblAltitude2")} (m):\n{iLoc.altitude.ToString()}";
                    else
                        lblAltitudeS.Text = $"{translations.Get_Translation("lblAltitude2")}(ft):\n{Math.Round(iLoc.altitude * 3.28084, 2).ToString()}";

                    if (iLoc.latitude.ToString().Length > 5)
                        lblLatitudeS.Text = $"{translations.Get_Translation("lblLatitude\\PlaceholderText")}:\n{iLoc.latitude.ToString().Substring(0, 5)}";
                    else
                        lblLatitudeS.Text = $"{translations.Get_Translation("lblLatitude\\PlaceholderText")}:\n{iLoc.latitude.ToString()}";

                    if (iLoc.latitude.ToString().Length > 5)
                        lblLongitudeS.Text = $"{translations.Get_Translation("lblLongitude\\Text")}:\n{iLoc.longitude.ToString().Substring(0, 5)}";
                    else
                        lblLongitudeS.Text = $"{translations.Get_Translation("lblLongitude\\Text")}:\n{iLoc.longitude.ToString()}";

                    if (profile.units == Units.Meters)
                    {
                        gravityR = double.Parse(GetGResult(iLoc.latitude, Convert.ToDouble(iLoc.altitude), Units.Meters));
                        lblUnitsG.Text = GetUResult(Units.Meters);
                    }
                    else
                    {
                        gravityR = double.Parse(GetGResult(iLoc.latitude, Convert.ToDouble(iLoc.altitude), Units.Feet));
                        lblUnitsG.Text = GetUResult(Units.Feet);
                    }

                    locationData.Visibility = Visibility.Visible;

                    lblGravityTotal.Text = gravityR.ToString();

                    gravityR = Math.Round(gravityR, profile.nDecimal);

                    lblGravity.Text = gravityR.ToString();

                    Change_Text_Size();

                    SetGravity();
                }
                catch
                {
                    Change_Text_Size_Error();
                    msgBox.btnOk.Invoked = OkLunchLocation;
                    lblUnitsG.Visibility = Visibility.Collapsed;
                    lblGravity.Text = translations.Get_Translation("errData");
                    msgBox.Show(translations.Get_Translation("errGPS"), "Error", true);
                }
                bar.Visibility = Visibility.Collapsed;
            }
            else
            {
                Change_Text_Size_Disable();
                lblUnitsG.Visibility = Visibility.Collapsed;
                lblGravity.Text = translations.Get_Translation("disabledLocation");
            }
        }
        private async void OkLunchLocation(IUICommand command)
        {
            await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-settings-location:"));
        }
        private async void SetGravity()
        {
            GravityDataDAO gDao = new GravityDataDAO();

            double gravityR = double.Parse(lblGravityTotal.Text);

            if (lblGravity.Text == " ft/s²")
                gravityR = G.ChangeToMetres(gravityR);

            int exist = await gDao.GetSpecificGravity(lblLatitude.Text, lblLongitude.Text, lblAltitude.Text);

            if (exist == 0)
            {
                GravityDataDAO.Insert(new GravityData() { altitude = int.Parse(lblAltitude.Text), gravity = gravityR, latitude = double.Parse(lblLatitude.Text), longitude = double.Parse(lblLongitude.Text), registered = DateTime.Now });
            }
        }
        private string GetUResult(Units units)
        {
            if (units == Units.Feet)
                return " ft/s²";
            else
                return " m/s²";
        }
        private string GetGResult(double latitude, double altitude, Units units)
        {
            lblResult.FontSize = 36;

            Gravity G = new Gravity(latitude, 0, altitude);
            double result = -1;

            if (units == Units.Feet)
                altitude = G.ChangeToMetres(altitude);

            result = G.GetGravity();

            if (result == 0)
                return translations.Get_Translation("errLatitude");
            else if (result == 1)
                return translations.Get_Translation("errAltitudeAbove");
            else if (result == 2)
                return translations.Get_Translation("errAltitudeBelow");
            else
            {
                lblResult.FontSize = 30;

                if (units == Units.Feet)
                    result = G.ChangeToFeet(result);
                return result.ToString();
            }
        }

        private async Task<ResultData> GetAltLatLng(double latitude, double longitude)
        {
            var response = await GetRequest($"http://api.geonames.org/srtm3JSON?lat={latitude}&lng={longitude}&username=fanmixco");

            return JsonConvert.DeserializeObject<ResultData>(response);
        }

        private async Task<InfoLocation> GetLocation()
        {
            Geolocator geolocator = new Geolocator();

            Geoposition geoposition = await geolocator.GetGeopositionAsync();
            double latitude = geoposition.Coordinate.Point.Position.Latitude;
            double longitude = geoposition.Coordinate.Point.Position.Longitude;
            int altitude = (int)geoposition.Coordinate.Point.Position.Altitude;

            if (altitude == 0)
            {
                if (NetworkInterface.GetIsNetworkAvailable())
                {
                    var deserializedLocations = await GetAltLatLng(latitude, longitude);

                    latitude = deserializedLocations.lat;
                    longitude = deserializedLocations.lng;
                    altitude = deserializedLocations.srtm3;
                }
                else
                {
                    geolocator.DesiredAccuracy = PositionAccuracy.High;

                    geoposition = await geolocator.GetGeopositionAsync();

                    latitude = geoposition.Coordinate.Point.Position.Latitude;
                    longitude = geoposition.Coordinate.Point.Position.Longitude;
                    altitude = (int)geoposition.Coordinate.Point.Position.Altitude;
                }
            }

            return new InfoLocation() { latitude = latitude, longitude = longitude, altitude = altitude };
        }
        private async Task GetProfile()
        {
            ProfileDAO pDao = new ProfileDAO();
            profile = await pDao.GetProfile();
        }

        private void Change_Text_Size()
        {
            lblGravity.TextAlignment = TextAlignment.Left;
            lblGravity.TextWrapping = TextWrapping.NoWrap;
            if ("Windows.Mobile" == AnalyticsInfo.VersionInfo.DeviceFamily)
            {
                lblGravity.FontSize = 60;
                lblUnitsG.FontSize = 24;
            }
            else
            {
                lblGravity.FontSize = 140;
                lblUnitsG.FontSize = 60;
            }
        }

        private void Change_Text_Size_Error()
        {
            bar.Visibility = Visibility.Collapsed;
            lblGravity.TextAlignment = TextAlignment.Center;
            lblGravity.TextWrapping = TextWrapping.NoWrap;
            if ("Windows.Mobile" == AnalyticsInfo.VersionInfo.DeviceFamily)
            {
                lblGravity.FontSize = 20;
            }
            else
            {
                lblGravity.FontSize = 50;
            }
        }

        private void Change_Text_Size_Disable()
        {
            bar.Visibility = Visibility.Collapsed;
            lblGravity.TextAlignment = TextAlignment.Center;
            lblGravity.TextWrapping = TextWrapping.Wrap;
            lblGravity.FontSize = 20;
        }

        private void Clean()
        {
            bar.Visibility = Visibility.Visible;

            lblGravity.Text = "";
            lblUnitsG.Text = "";
            locationData.Visibility = Visibility.Collapsed;
        }
        private void pvtOptions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Started)
            {
                cBarOptions.ClosedDisplayMode = AppBarClosedDisplayMode.Minimal;
                menuChangeUnits.Visibility = Visibility.Collapsed;
                menuRefresh.Visibility = Visibility.Collapsed;
                menuShare.Visibility = Visibility.Collapsed;
                switch (pvtOptions.SelectedIndex)
                {
                    case 0:
                        cBarOptions.ClosedDisplayMode = AppBarClosedDisplayMode.Compact;
                        menuChangeUnits.Visibility = Visibility.Visible;
                        menuRefresh.Visibility = Visibility.Visible;
                        menuShare.Visibility = Visibility.Visible;
                        btnPin.Visibility = Visibility.Visible;
                        btnDonation.Visibility = Visibility.Visible;
                        break;
                    case 2:
                        cBarOptions.ClosedDisplayMode = AppBarClosedDisplayMode.Compact;
                        menuShare.Visibility = Visibility.Visible;
                        btnPin.Visibility = Visibility.Visible;
                        btnDonation.Visibility = Visibility.Visible;
                        break;
                }
            }
            int pivotS = pvtOptions.SelectedIndex;

        }
        private void txtValue_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txtVal = (TextBox)sender;
            if (txtVal.Text.Equals(",") || txtVal.Text.Equals(".") || txtVal.Text.Equals("-"))
                return;

            bool res = double.TryParse(txtVal.Text, out double num1);
            if (res == false)
            {
                lblResult.Visibility = Visibility.Visible;
                lblUnitsC.Visibility = Visibility.Collapsed;
                lblResult.Text = translations.Get_Translation("lblInvalidN");
            }
            else
                CalculateGravity();
        }

        private void CalculateGravity()
        {
            try
            {
                if (!(string.IsNullOrEmpty(txtLatitude.Text) || string.IsNullOrEmpty(txtAltitude.Text)) && cmbUnits.SelectedIndex > -1)
                {
                    lblResult.Visibility = Visibility.Visible;
                    lblUnitsC.Visibility = Visibility.Visible;

                    double gravityR = 0;
                    string tempGravity = "";

                    if (cmbUnits.SelectedIndex == 0)
                    {
                        tempGravity = GetGResult(Convert.ToDouble(txtLatitude.Text), Convert.ToDouble(txtAltitude.Text), Units.Meters);
                        lblUnitsC.Text = GetUResult(Units.Meters);
                    }
                    else
                    {
                        tempGravity = GetGResult(Convert.ToDouble(txtLatitude.Text), G.ChangeToMetres(Convert.ToDouble(txtAltitude.Text)), Units.Feet);
                        lblUnitsC.Text = GetUResult(Units.Feet);
                    }

                    string pattern = "^[-+]?[0-9]*\\.?[0-9]*$";

                    if (Regex.IsMatch(tempGravity, pattern) == false)
                    {
                        lblUnitsC.Visibility = Visibility.Collapsed;
                        lblUnitsC.Text = "";
                    }
                    else
                    {
                        gravityR = Math.Round(Convert.ToDouble(tempGravity), profile.calcDecimal);
                        tempGravity = gravityR.ToString();
                    }

                    lblResult.Text = tempGravity;
                }
            }
            catch
            {

                lblResult.Visibility = Visibility.Visible;
                lblUnitsC.Visibility = Visibility.Collapsed;
                lblResult.Text = translations.Get_Translation("lblInvalidN");
            }
        }
        private void cmbUnits_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Calculate();
        }

        private void Calculate()
        {
            try
            {
                if (!(string.IsNullOrEmpty(txtLatitude.Text) || string.IsNullOrEmpty(txtAltitude.Text)) && cmbUnits.SelectedIndex > -1)
                {
                    lblResult.Visibility = Visibility.Visible;
                    lblUnitsC.Visibility = Visibility.Visible;

                    double gravityR = 0;
                    string tempGravity = "";

                    if (cmbUnits.SelectedIndex == 0)
                    {
                        tempGravity = GetGResult(Convert.ToDouble(txtLatitude.Text), Convert.ToDouble(txtAltitude.Text), Units.Meters);
                        lblUnitsC.Text = GetUResult(Units.Meters);
                    }
                    else
                    {
                        tempGravity = GetGResult(Convert.ToDouble(txtLatitude.Text), G.ChangeToMetres(Convert.ToDouble(txtAltitude.Text)), Units.Feet);
                        lblUnitsC.Text = GetUResult(Units.Feet);
                    }

                    string pattern = "^[-+]?[0-9]*\\.?[0-9]*$";

                    if (Regex.IsMatch(tempGravity, pattern) == false)
                    {
                        lblUnitsC.Visibility = Visibility.Collapsed;
                        lblUnitsC.Text = "";
                    }
                    else
                    {
                        gravityR = Math.Round(Convert.ToDouble(tempGravity), profile.calcDecimal);
                        tempGravity = gravityR.ToString();
                    }

                    lblResult.Text = tempGravity;
                }
            }
            catch
            {

                lblResult.Visibility = Visibility.Visible;
                lblUnitsC.Visibility = Visibility.Collapsed;
                lblResult.Text = translations.Get_Translation("lblInvalidN");
            }
        }

        private void cmbUnitsComp_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!Started)
                return;

            if (cmbUnitsComp.SelectedIndex == 0)
            {
                lblGravity1.Text = G.ChangeToMetres(Convert.ToDouble(lblGravity1.Text)).ToString();

                if (!string.IsNullOrEmpty(lblGravity2.Text))
                    lblGravity2.Text = G.ChangeToMetres(Convert.ToDouble(lblGravity2.Text)).ToString();
            }
            else
            {
                lblGravity1.Text = G.ChangeToFeet(Convert.ToDouble(lblGravity1.Text)).ToString();

                if (!string.IsNullOrEmpty(lblGravity2.Text))
                    lblGravity2.Text = G.ChangeToFeet(Convert.ToDouble(lblGravity2.Text)).ToString();
            }
        }

        private void cmbPlaces_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!Started)
                return;

            if (cmbUnitsComp.SelectedIndex == -1)
                return;

            ComboBox cmbValue = (ComboBox)sender;

            double gValue;
            if (!((string.Equals(lblGravity.Text, translations.Get_Translation("errData"))) || string.IsNullOrEmpty(lblGravity.Text)) && cmbValue.SelectedIndex == 3)
                gValue = Convert.ToDouble(lblGravity.Text);
            else
                gValue = data.GetGravity(cmbValue.SelectedIndex);

            if (cmbUnitsComp.SelectedIndex == 1)
                gValue = G.ChangeToFeet(gValue);

            switch (cmbValue.Name)
            {
                case "cmbPlaces1":
                    imgPlace1.Source = new BitmapImage(new Uri("ms-appx:///Assets/Planets/" + data.GetName(cmbValue.SelectedIndex).ToString().ToLower() + ".jpg"));
                    lblDescription1.Text = data.GetDescription(cmbValue.SelectedIndex);
                    lblGravity1.Text = gValue.ToString();
                    break;
                case "cmbPlaces2":
                    imgPlace2.Source = new BitmapImage(new Uri("ms-appx:///Assets/Planets/" + data.GetName(cmbValue.SelectedIndex).ToString().ToLower() + ".jpg"));
                    lblDescription2.Text = data.GetDescription(cmbValue.SelectedIndex);
                    lblGravity2.Text = gValue.ToString();
                    break;
            }

            if (cmbPlaces1.SelectedIndex > -1 && cmbPlaces2.SelectedIndex > -1)
            {
                string extra = "";

                if (cmbPlaces1.SelectedIndex == 3 || cmbPlaces1.SelectedIndex == 4)
                    extra = translations.Get_Translation("thePlus") + " ";

                int gCompared = data.ComparedGravity(cmbPlaces1.SelectedIndex, cmbPlaces2.SelectedIndex);
                double gPercentage = 0;

                gPercentage = data.PercentageGravity(cmbPlaces1.SelectedIndex, cmbPlaces2.SelectedIndex);

                switch (gCompared)
                {
                    case 0:
                        lblCompared1.Foreground = new SolidColorBrush(Colors.Green);
                        lblCompared1.Text = string.Format(translations.Get_Translation("gravBigger"), extra + (cmbPlaces1.SelectedItem as ComboBoxItem).Content.ToString()) + Math.Round(gPercentage, 0).ToString() + " % ";
                        break;
                    case 1:
                        lblCompared1.Foreground = new SolidColorBrush(Colors.Red);
                        lblCompared1.Text = string.Format(translations.Get_Translation("gravSmaller"), extra + (cmbPlaces1.SelectedItem as ComboBoxItem).Content.ToString()) + Math.Round(gPercentage, 0).ToString() + " % ";
                        break;
                    case 2:
                        lblCompared1.Foreground = new SolidColorBrush(Colors.Blue);
                        lblCompared1.Text = translations.Get_Translation("gravSame");
                        break;
                }
            }
            CalculateWeight();
        }

        private void CalculateWeight()
        {
            bool isNumeric = double.TryParse(txtYourWeight.Text, out double earthWeight);
            if (!string.IsNullOrEmpty(txtYourWeight.Text) && isNumeric)
            {
                var weight = earthWeight;
                double newWeight;
                var gravity1 = data.GetGravity(cmbPlaces1.SelectedIndex);
                var gravity2 = data.GetGravity(cmbPlaces2.SelectedIndex);

                var difference = gravity2 / gravity1;

                if (cmbPlaces1.SelectedIndex != 3)
                {
                    var temp = data.GetGravity(cmbPlaces1.SelectedIndex) / data.GetGravity(3);
                    newWeight = weight * temp;

                    newWeight *= difference;
                }
                else
                {

                    newWeight = weight * difference;
                }
                Set_Weight(weight, Math.Round(newWeight, 1));
            }
        }

        private void Set_Weight(double initialWeight, double newWeight)
        {
            if (newWeight < initialWeight)
            {
                lblWeightCompared.Foreground = new SolidColorBrush(Colors.Green);
                lblWeightCompared.Text = string.Format(translations.Get_Translation("lightWeight"), (cmbPlaces2.SelectedItem as ComboBoxItem).Content.ToString(), newWeight, (cmbUnitsWeightComp.SelectedItem as ComboBoxItem).Content.ToString());
                imgWeight.Source = new BitmapImage(new Uri("ms-appx:///Assets/AppBar/light.png"));
            }
            else if (newWeight > initialWeight)
            {
                lblWeightCompared.Foreground = new SolidColorBrush(Colors.Red);
                lblWeightCompared.Text = string.Format(translations.Get_Translation("heavyWeight"), (cmbPlaces2.SelectedItem as ComboBoxItem).Content.ToString(), newWeight, (cmbUnitsWeightComp.SelectedItem as ComboBoxItem).Content.ToString());
                imgWeight.Source = new BitmapImage(new Uri("ms-appx:///Assets/AppBar/weight.png"));
            }
            else
            {
                lblWeightCompared.Foreground = new SolidColorBrush(Colors.Blue);
                lblWeightCompared.Text = translations.Get_Translation("sameWeight");
                imgWeight.Source = null;
            }
        }

        public async Task<string> GetRequest(string URL)
        {
            Uri geturi = new Uri(URL); //replace your url  
            HttpClient client = new HttpClient();
            HttpResponseMessage responseGet = await client.GetAsync(geturi);
            return await responseGet.Content.ReadAsStringAsync();
        }

        private void lstGravities_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ListView item = (ListView)sender;

            if (item.SelectedItem != null)
            {
                var gravity = item.SelectedItem as GravityOSM;

                var latitude = gravity.latitude.Replace(translations.Get_Translation("lblLatitude\\PlaceholderText") + ": ", "");
                latitude = latitude.Replace("°", "");

                var longitude = gravity.longitude.Replace(translations.Get_Translation("lblLongitude\\Text") + ": ", "");
                longitude = longitude.Replace("°", "");

                var gravityD = gravity.gravity.Replace(translations.Get_Translation("lblGravity2") + " ", "");
                gravityD = gravityD.Replace("m/s²", "");

                Frame.Navigate(typeof(Map), new Tuple<string, string, string, string, Units>(gravity.altitude, latitude, longitude, gravityD, Units.Meters));
            }
        }

        private void menuAbout_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Pages.AboutUs));
        }

        private void menuHelp_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Pages.Help));
        }

        private void menuProfile_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Pages.Profile));
        }

        private void menuChangeUnits_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if ((string.Equals(lblGravity.Text, translations.Get_Translation("errData"))) || string.IsNullOrEmpty(lblGravity.Text))
                return;

            double gravity;
            string units;
            int pivotS = pvtOptions.SelectedIndex;
            double altitude = Convert.ToDouble(lblAltitude.Text);
            string cUnits = " (m)";
            if (pivotS == 0)
            {
                if (lblUnitsG.Text == " m/s²")
                {
                    gravity = Math.Round(G.ChangeToFeet(Convert.ToDouble(lblGravity.Text)), profile.nDecimal);
                    units = GetUResult(Units.Feet);

                    altitude = Math.Round(G.ChangeToFeet(altitude), 0);
                    cUnits = " (ft)";
                }
                else
                {
                    gravity = Math.Round(G.ChangeToMetres(Convert.ToDouble(lblGravity.Text)), profile.nDecimal);
                    units = GetUResult(Units.Meters);

                    altitude = Math.Round(G.ChangeToMetres(altitude), 0);
                }

                lblAltitudeS.Text = translations.Get_Translation("lblAltitude\\PlaceholderText") + cUnits + ":\n" + altitude.ToString();
                lblUnitsG.Text = units;
                lblGravity.Text = gravity.ToString();
            }
        }

        private void menuRefresh_Tapped(object sender, TappedRoutedEventArgs e)
        {
            lblGravity.TextAlignment = TextAlignment.Left;
            lblGravity.Text = "";
            Change_Text_Size();
            GetSinglePositionAsync();
        }

        private string ShareGravity()
        {
            if ((string.Equals(lblGravity.Text, translations.Get_Translation("errData"))) || string.IsNullOrEmpty(lblGravity.Text))
                return null;

            return $"{lblGravity.Text} {lblUnitsG.Text}";
        }

        private void menuEmail_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (pvtOptions.SelectedIndex == 0)
                Share_Gravity_Email();
            else
                Share_Weight_Email();
        }

        private void Share_Weight_Email()
        {
            if (cmbPlaces2.SelectedIndex != 3)
            {
                string subject = translations.Get_Translation("lblSubject");

                ShareOptions.Share_Email(null, translations.Get_Translation("weightSubject"), lblWeightCompared.Text + "\n\r\n\r" + translations.Get_Translation("downloadGNow") + "\n\rhttps://www.microsoft.com/store/apps/9nblgggzjlp5");
            }
        }

        private void Share_Gravity_Email()
        {
            var gravity = ShareGravity();

            if (gravity == null)
                return;

            //string body = translations.Get_Translation("lblBody1") + gravity + translations.Get_Translation("lblBody2");
            string subject = translations.Get_Translation("lblSubject");

            ShareOptions.Share_Email(null, subject, $"{lblWeightCompared.Text}\n\r\n\r{translations.Get_Translation("downloadGNow")}\n\rhttps://www.microsoft.com/store/apps/9nblgggzjlp5");
        }
        private void Share_Gravity_SMS()
        {
            var gravity = ShareGravity();

            if (gravity == null)
                return;

            string body = translations.Get_Translation("lblBody1") + gravity + translations.Get_Translation("lblBody2");

            ShareOptions.Share_SMS($"{body}\n\r\n\r{translations.Get_Translation("downloadGNow")}");
        }

        private void menuSMS_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (pvtOptions.SelectedIndex == 0)
                Share_Gravity_SMS();
            else
                Share_Weight_SMS();
        }

        private void Share_Weight_SMS()
        {
            if (cmbPlaces2.SelectedIndex != 3)
            {
                ShareOptions.Share_SMS($"{lblWeightCompared.Text}\n\r\n\r{translations.Get_Translation("downloadGNow")}");
            }
        }

        private void btnDonation_Tapped(object sender, TappedRoutedEventArgs e)
        {
            CheckDonation();
        }

        private async void CheckDonation()
        {
            MessageBox msgBox = new MessageBox();

            string answer = "";

            try
            {
                bool result = await Supernova.Core.InAppPurchases.Check_InApp("DonationGNow");
                if (result)
                {
                    answer = translations.Get_Translation("ThankYouInApp");
                    btnDonation.Visibility = Visibility.Collapsed;
                    gridAds.Visibility = Visibility.Collapsed;
                }
                else
                {
                    answer = translations.Get_Translation("NoInApp");
                }
            }
            catch (Exception ex) { answer = ex.Message; }
            msgBox.Show(answer);
        }

        private void TextBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {
            CheckDonation();
        }

        private bool CheckSecondaryTile()
        {
            return SecondaryTile.Exists(appbarTileId);
        }
        private void ChangePinInfo()
        {
            string pin = "ms-appx:///Assets/AppBar/pinned.png";
            string pinContent = translations.Get_Translation("btnPin");
            if (CheckSecondaryTile())
            {
                pin = "ms-appx:///Assets/AppBar/unpinned.png";
                pinContent = translations.Get_Translation("btnUnpin");
            }
            btnPin.Icon = new BitmapIcon() { UriSource = new Uri(pin) };
            btnPin.Label = pinContent;
        }
        private async void btnPin_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (!CheckSecondaryTile())
            {
                SecondaryTile tileData = new SecondaryTile();
                tileData.TileId = appbarTileId;
                tileData.Arguments = "Something";
                tileData.VisualElements.Square71x71Logo = new Uri("ms-appx:///Assets/Square71x71Logo.scale-240.png");
                tileData.VisualElements.Square150x150Logo = new Uri("ms-appx:///Assets/Logo.scale-240.png");
                tileData.DisplayName = translations.Get_Translation("AppTitle\\Title");
                tileData.VisualElements.ShowNameOnSquare150x150Logo = true;
                await tileData.RequestCreateAsync();
            }
            else
            {
                var tiles = await SecondaryTile.FindAllAsync();
                var tileToDelete = tiles.FirstOrDefault(tile => tile.TileId == appbarTileId);
                if (tileToDelete != null)
                {
                    await tileToDelete.RequestDeleteAsync();
                }
            }
            ChangePinInfo();
        }

        private void cmbUnitsWeightComp_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!Started)
                return;

            if (cmbPlaces2.SelectedIndex >= 0)
                CalculateWeight();
        }

        private void txtYourWeight_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!Started)
                return;

            if (cmbPlaces2.SelectedIndex >= 0)
                CalculateWeight();
        }

        private async void txtLocation_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtLocation.Text.Length < 4)
                return;

            List<GravityOSM> gOSM = new List<GravityOSM>();
            msgBox = new MessageBox();
            if (NetworkInterface.GetIsNetworkAvailable())
            {
                string address = txtLocation.Text;

                if (string.IsNullOrEmpty(address))
                    return;

                var response = await GetRequest($"https://nominatim.openstreetmap.org/search?q={address}&format=json&email=fanmixco@gmail.com");

                try
                {
                    var deserialized = JsonConvert.DeserializeObject<List<ResultLocation>>(response);

                    foreach (var location in deserialized)
                    {
                        var deserializedLocations = await GetAltLatLng(double.Parse(location.lat), double.Parse(location.lon));

                        Gravity G = new Gravity(deserializedLocations.lat, deserializedLocations.lng, deserializedLocations.srtm3);

                        gOSM.Add(new GravityOSM()
                        {
                            altitude = deserializedLocations.srtm3.ToString(),
                            latitude = $"{translations.Get_Translation("lblLatitude\\PlaceholderText")}: {location.lat}°",
                            longitude = $"{translations.Get_Translation("lblLongitude\\Text")}: {location.lon}°",
                            address = $"{translations.Get_Translation("lblLocation")}: {location.display_name}",
                            gravity = $"{translations.Get_Translation("lblGravity2")} {Math.Round(G.GetGravity(), 6).ToString()} m/s²"
                        });
                    }
                    lstGravities.ItemsSource = gOSM;
                }
                catch
                {
                    msgBox.Show(translations.Get_Translation("lblTryAgain"), "Error");
                }
            }
            else
                msgBox.Show(translations.Get_Translation("errInternet"), "Error");
        }
    }
}
