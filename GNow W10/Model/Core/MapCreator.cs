﻿using System;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Media.Imaging;

namespace Supernova.Core
{
    class MapCreator
    {
        private MapControl map;
        private double latitude;
        private double longitude;
        private int? idRuin;
        private int? idCountry;

        public MapCreator(MapControl obj)
        {
            map = obj;
        }

        public void SetCenter(double lat, double lng, double zoom)
        {
            latitude = lat;
            longitude = lng;

            map.Center = new Geopoint(new BasicGeoposition() { Latitude = lat, Longitude = lng });
            map.ZoomLevel = zoom;
        }

        public void AddPushPin(double latitude, double longitude, string name = "")
        {
            Image iconStart = new Image();
            iconStart.Source = new BitmapImage(new Uri("ms-appx:///Assets/AppBar/marker.png"));
            iconStart.Height = 30;
            iconStart.Width = 30;
            map.Children.Add(iconStart);
            MapControl.SetLocation(iconStart, new Geopoint(new BasicGeoposition()
            {
                Latitude = latitude,
                Longitude = longitude
            }));

            MapControl.SetNormalizedAnchorPoint(iconStart, new Point(0.5, 0.5));
        }
    }
}
