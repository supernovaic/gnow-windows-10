﻿using System.Collections.Generic;

namespace Supernova.GNow
{
    class Planets
    {
        public int idplace;
        public string name;
        public string description;
        public double gravity;
    }

    public class GravityPlanets
    {
        List<Planets> place = new List<Planets>();

        public GravityPlanets()
        {
            place.Add(new Planets() { idplace = 0, name = "Sun", description = "star", gravity = 274 });
            place.Add(new Planets() { idplace = 1, name = "Mercury", description = "planet", gravity = 3.7 });
            place.Add(new Planets() { idplace = 2, name = "Venus", description = "planet", gravity = 8.87 });
            place.Add(new Planets() { idplace = 3, name = "Earth", description = "planet", gravity = 9.798 });
            place.Add(new Planets() { idplace = 4, name = "Moon", description = "moon", gravity = 1.62 });
            place.Add(new Planets() { idplace = 5, name = "Mars", description = "planet", gravity = 3.71 });
            place.Add(new Planets() { idplace = 6, name = "Jupiter", description = "planet", gravity = 24.92 });
            place.Add(new Planets() { idplace = 7, name = "Saturn", description = "planet", gravity = 10.44 });
            place.Add(new Planets() { idplace = 8, name = "Uranus", description = "planet", gravity = 8.69 });
            place.Add(new Planets() { idplace = 9, name = "Neptune", description = "planet", gravity = 11.15 });
            place.Add(new Planets() { idplace = 10, name = "Pluto", description = "dwarf planet", gravity = 0.58 });
        }

        public int ComparedGravity(int id1, int id2)
        {
            double gravity1 = place[id1].gravity;
            double gravity2 = place[id2].gravity;

            if (gravity1 > gravity2)
                return 0;
            else if (gravity1 < gravity2)
                return 1;
            else
                return 2;
        }

        public double PercentageGravity(int id1, int id2)
        {
            int result = ComparedGravity(id1, id2);

            double gravity1 = place[id1].gravity;
            double gravity2 = place[id2].gravity;

            if (result == 0)
                return (gravity1 * 100) / gravity2;
            else if (result == 1)
                return (gravity2 * 100) / gravity1;
            else
                return 1;
        }

        public string GetName(int id)
        {
            return place[id].name;
        }

        public string GetDescription(int id)
        {
            return place[id].description;
        }

        public double GetGravity(int id)
        {
            return place[id].gravity;
        }
    }
}
