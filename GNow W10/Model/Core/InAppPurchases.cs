﻿using System;
using System.Threading.Tasks;
using Windows.ApplicationModel.Store;

namespace Supernova.Core
{
    class InAppPurchases
    {
        public static async Task<bool> Check_InApp(string inApp)
        {
            if (CurrentApp.LicenseInformation.ProductLicenses[inApp].IsActive)
            {
                return true;
            }
            var results = await CurrentApp.RequestProductPurchaseAsync(inApp);

            if (results.Status == ProductPurchaseStatus.Succeeded ||
                results.Status == ProductPurchaseStatus.AlreadyPurchased)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool Check_Status(string inApp)
        {
            ProductLicense productLicense = null;
            var result = CurrentApp.LicenseInformation.ProductLicenses.TryGetValue(inApp, out productLicense);

            if (result)
            {
                if (productLicense.IsActive)
                    return true;
            }
            return false;
        }
    }
}
