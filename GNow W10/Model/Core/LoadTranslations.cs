﻿using Windows.ApplicationModel.Resources;

namespace Supernova.Model.Database
{
    class LoadTranslations
    {
        ResourceLoader loader = new ResourceLoader();

        public string Get_Translation(string resource, string source = null)
        {
            resource = resource.Replace(".", "\\");

            if (source == null)
                return loader.GetString(resource);
            else
                return ResourceLoader.GetForCurrentView(source).GetString(resource);
        }

        public string Get_First_UpperCase(string resource)
        {
            return UppercaseFirst(Get_Translation(resource));
        }
        private static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }
    }
}
