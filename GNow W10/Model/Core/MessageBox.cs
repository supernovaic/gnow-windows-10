﻿using System;
using Supernova.Model.Database;
using Windows.UI.Popups;

namespace Stop_Diabetes.Supernova.Messages
{
    class MessageBox
    {
        //LoadTranslations translations;
        LoadTranslations translations = new LoadTranslations();

        public UICommand btnOk;
        public UICommand btnCancel;

        public MessageBox()
        {
            //translations = new LoadTranslations();
            btnOk = new UICommand(translations.Get_Translation("btnOK"));
            btnCancel = new UICommand(translations.Get_First_UpperCase("btnCancel"));
        }
        public async void Show(string msg, string title = "", bool buttons = false)
        {
            MessageDialog messageDialog;
            if (String.IsNullOrEmpty(title))
                messageDialog = new MessageDialog(msg);
            else
                messageDialog = new MessageDialog(msg, title.ToUpper());

            if (buttons)
            {
                messageDialog.Commands.Add(btnOk);
                messageDialog.Commands.Add(btnCancel);
            }

            await messageDialog.ShowAsync();
        }
    }
}
