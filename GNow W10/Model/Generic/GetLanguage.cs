﻿using System.Globalization;

namespace Supernova.Model.Generic
{
    class GetLanguage
    {
        public static string GetCurrentLanguage()
        {
            CultureInfo ci = CultureInfo.CurrentCulture;

            var language = ci.TwoLetterISOLanguageName;

            if (language != "en" || language != "es")
                language = "en";

            return language;
        }
    }
}
