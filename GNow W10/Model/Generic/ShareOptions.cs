﻿using System;
using Windows.ApplicationModel.Chat;
using Windows.ApplicationModel.Email;

namespace Supernova.Model.Generic
{
    class ShareOptions
    {
        public static async void Share_SMS(string SMS)
        {
            var message = new ChatMessage();
            message.Body = SMS;

            await ChatMessageManager.ShowComposeSmsMessageAsync(message);
        }

        public static async void Share_Email(string email, string subject, string message)
        {
            EmailMessage mail = new EmailMessage();
            mail.Subject = subject;
            if (email != null)
                mail.To.Add(new EmailRecipient() { Address = email });
            mail.Body = message;

            await EmailManager.ShowComposeNewEmailAsync(mail);
        }
    }
}
