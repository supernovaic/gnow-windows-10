﻿using System;
using Windows.System;

namespace Supernova.Model.Generic
{
    class BrowserOptions
    {
        public static async void LunchURL(string uriToLaunch)
        {
            var uri = new Uri(uriToLaunch);
            // Set the option to show a warning
            var options = new Windows.System.LauncherOptions();
            options.TreatAsUntrusted = true;

            // Launch the URI with a warning prompt
            var success = await Launcher.LaunchUriAsync(uri, options);
        }
    }
}
