﻿using System;
using System.Threading.Tasks;
using Windows.ApplicationModel.Store;

namespace Stop_Diabetes.Supernova
{
    class InAppPurchases
    {
        public static async Task<bool> Check_InApp(string inApp)
        {
            if (CurrentApp.LicenseInformation.ProductLicenses[inApp].IsActive)
            {
                return true;
            }
            var results = await CurrentApp.RequestProductPurchaseAsync(inApp);

            if (results.Status == ProductPurchaseStatus.Succeeded ||
                results.Status == ProductPurchaseStatus.AlreadyPurchased)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
