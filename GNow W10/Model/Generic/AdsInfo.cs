﻿namespace Supernova.Model.Generic
{
    class AdsInfo
    {
        private const string ApplicationID = "eabc792a-c2d3-48ae-a181-c8940cb5ac73";
        private const string ApplicationIDMobile = "28be9d7d-d0e2-4495-b095-26e7cdc8c37c";
        private const string AdUnitID = "316304";
        private const string AdUnitIDMobile = "316305";

        private string WindowsVersion;

        public AdsInfo()
        {
            WindowsVersion = Windows.System.Profile.AnalyticsInfo.VersionInfo.DeviceFamily;
        }

        public int GetHeight()
        {
            if (WindowsVersion == "Windows.Desktop")
            {
                return 90;
            }
            else if (WindowsVersion == "Windows.Mobile")
            {
                return 50;
            }
            else
                return 90;
        }

        public int GetWidth()
        {
            if (WindowsVersion == "Windows.Desktop")
            {
                return 728;
            }
            else if (WindowsVersion == "Windows.Mobile")
            {
                return 320;
            }
            else
                return 728;
        }

        public string GetAdsUnit()
        {
            if (WindowsVersion == "Windows.Desktop")
            {
                return AdUnitID;
            }
            else if (WindowsVersion == "Windows.Mobile")
            {
                return AdUnitIDMobile;
            }
            else
                return AdUnitID;
        }

        public string GetApplicationID()
        {
            if (WindowsVersion == "Windows.Desktop")
            {
                return ApplicationID;
            }
            else if (WindowsVersion == "Windows.Mobile")
            {
                return ApplicationIDMobile;
            }
            else
                return ApplicationID;
        }
    }
}