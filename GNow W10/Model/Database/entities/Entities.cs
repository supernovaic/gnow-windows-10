﻿using SQLite;
using System;

namespace Supernova.Entities
{
    public class Profile
    {
        [PrimaryKey, AutoIncrement]
        public int idUser { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public Units units { get; set; }
        public bool sync { get; set; }
        public int nDecimal { get; set; }
        public int calcDecimal { get; set; }
    }

    class GravityData
    {
        [PrimaryKey, AutoIncrement]

        public int idGravity { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public int altitude { get; set; }
        public double gravity { get; set; }
        public DateTime registered { get; set; }
    }

    #region Enums

    public enum Units
    {
        Meters,
        Feet
    }
    #endregion
}