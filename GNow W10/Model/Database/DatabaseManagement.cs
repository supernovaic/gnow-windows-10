﻿using SQLite;
using Supernova.Entities;
using System;
using System.IO;
using System.Threading.Tasks;
using Supernova.Model.Database.dao;
using Windows.Storage;

namespace Supernova.Database
{
    public class DatabaseManagement
    {
        protected static SQLiteAsyncConnection ConnectionDb()
        {
            var conn = new SQLiteAsyncConnection(Path.Combine(ApplicationData.Current.LocalFolder.Path, "tipsal.db"), true);
            return conn;
        }
        public static async Task CreateDatabase()
        {
            var profile = ConnectionDb().CreateTableAsync<Profile>();
            var gravityData = ConnectionDb().CreateTableAsync<GravityData>();

            await Task.WhenAll(new Task[] { profile, gravityData });
            await Task.WhenAll(new Task[] { CreateProfile() });
            var localSettings = ApplicationData.Current.LocalSettings;

            localSettings.Values["FirstRun"] = DateTime.Now.ToString();
        }
        public async static Task CreateProfile()
        {
            ProfileDAO.Insert(new Profile() { email = "", sync = false, calcDecimal = 8, units = Units.Meters, nDecimal = 3, name = "" });
        }
    }
}
