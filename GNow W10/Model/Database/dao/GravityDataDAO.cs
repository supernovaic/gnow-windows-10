﻿using SQLite;
using Supernova.Database;
using Supernova.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Supernova.Model.Database.dao
{
    class GravityDataDAO : DatabaseManagement
    {
        static SQLiteAsyncConnection con;
        public GravityDataDAO()
        {
            con = ConnectionDb();
        }
        public static void Delete(int id)
        {
            
        }
        public async static void Insert(GravityData obj)
        {
            new GravityDataDAO();
            await con.InsertAsync(obj);
        }

        public async static void Update(GravityData obj)
        {
            new GravityDataDAO();
            await con.UpdateAsync(obj);
        }
        public async Task<List<GravityData>> GetAllData(string id = "")
        {
            string query = "select * from GravityData";
            if (id != null)
                query += " where idGravity=" + id;
            var result = await con.QueryAsync<GravityData>(query);
            return result;
        }
        public async Task<List<GravityData>> GetAllData()
        {
            string query = "SELECT idgravity, round(latitude,6) latitude, round(longitude,6) longitude, altitude, round(gravity,6) gravity FROM GravityData ORDER BY registered DESC";
            var result = await con.QueryAsync<GravityData>(query);
            return result;
        }
        public async Task<int> GetSpecificGravity(string lat, string lng, string alt)
        {
            string query = string.Format("select * from GravityData where latitude={0} and longitude={1} and altitude={2}", lat, lng, alt);
            var result = await con.QueryAsync<GravityData>(query);
            return result.Count;
        }
    }
}
