﻿using SQLite;
using Supernova.Database;
using Supernova.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Supernova.Model.Database.dao
{
    class ProfileDAO : DatabaseManagement
    {
        static SQLiteAsyncConnection con;
        public ProfileDAO()
        {
            con = ConnectionDb();
        }
        public static void Delete(int id)
        {
            
        }
        public async static void Insert(Profile obj)
        {
            new ProfileDAO();
            await con.InsertAsync(obj);
        }

        public async static void Update(Profile obj)
        {
            new ProfileDAO();
            await con.UpdateAsync(obj);
        }
        public async Task<Profile> GetProfile()
        {
            string query = "select * from Profile";
            var result = await con.QueryAsync<Profile>(query);
            return result.SingleOrDefault();
        }
    }
}
