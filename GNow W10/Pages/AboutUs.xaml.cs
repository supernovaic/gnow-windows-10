﻿using Supernova.Model.Generic;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace GNow_W10.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AboutUs : Page
    {
        public AboutUs()
        {
            this.InitializeComponent();
        }

        private void imgSpaceApps_Tapped(object sender, TappedRoutedEventArgs e)
        {
            BrowserOptions.LunchURL("https://2014.spaceappschallenge.org/awards/#globalawards");
        }

        private void imgNASA_Tapped(object sender, TappedRoutedEventArgs e)
        {
            BrowserOptions.LunchURL("https://www.nasa.gov");
        }

        private void txtEmail_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ShareOptions.Share_Email(txtEmail.Text, "Suggestions or comments GNow", "");
        }

        private void txtSite_Tapped(object sender, TappedRoutedEventArgs e)
        {
            BrowserOptions.LunchURL("http://" + txtSite.Text);
        }

        private void lblSensorOne_Tapped(object sender, TappedRoutedEventArgs e)
        {
            BrowserOptions.LunchURL("http://www.sensorsone.com/");
        }

        private void lblMSPs_Tapped(object sender, TappedRoutedEventArgs e)
        {
            BrowserOptions.LunchURL("http://www.microsoftstudentpartners.com/");
        }

        private void lblOSM_Tapped(object sender, TappedRoutedEventArgs e)
        {
            BrowserOptions.LunchURL("http://www.openstreetmap.org");
        }

        private void lblGeoNames_Tapped(object sender, TappedRoutedEventArgs e)
        {
            BrowserOptions.LunchURL("http://www.sensorsone.com/");
        }
    }
}
