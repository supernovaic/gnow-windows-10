﻿using Supernova.Model.Database;
using Supernova.Model.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace GNow_W10.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Help : Page
    {
        private LoadTranslations translations;
        public Help()
        {
            this.InitializeComponent();
            translations = new LoadTranslations();
        }

        private void HyperlinkButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            BrowserOptions.LunchURL("http://en.wikipedia.org/wiki/International_Gravity_Formula");
        }

        private void HyperlinkButton_Tapped_1(object sender, TappedRoutedEventArgs e)
        {
            BrowserOptions.LunchURL("http://en.wikipedia.org/wiki/Gravity_of_Earth#Free_air_correction");
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            Paragraph newP = new Paragraph();
            Paragraph newP2 = new Paragraph();
            Paragraph newP3 = new Paragraph();
            Paragraph newP4 = new Paragraph();
            Paragraph newP5 = new Paragraph();
            Paragraph newP6 = new Paragraph();

            Run run = new Run();
            run.Text = translations.Get_Translation("lblGravityDesc");

            newP.Inlines.Add(run);

            rtxtDescGravity.Blocks.Add(newP);

            run = new Run();
            run.Text = translations.Get_Translation("lblGravityCDesc");

            newP2.Inlines.Add(run);

            rtxtGravityCDesc.Blocks.Add(newP2);

            newP3.TextAlignment = TextAlignment.Justify;

            run = new Run();
            run.Text = translations.Get_Translation("lblFormula1");

            newP3.Inlines.Add(run);

            run = new Run();
            run.Text = "\n\r";

            newP3.Inlines.Add(run);

            newP4.TextAlignment = TextAlignment.Center;

            run = new Run();
            run.Text = "IGF = 9.780327 (1 + 0.0053024sin²Φ - 0.0000058sin²2Φ)";
            newP4.Inlines.Add(run);

            run = new Run();
            run.Text = "\n\r";
            newP4.Inlines.Add(run);

            run = new Run();
            run.Text = "FAC = -3.086 x 10⁻⁶ x h";
            newP4.Inlines.Add(run);

            run = new Run();
            run.Text = "\n\r";
            newP4.Inlines.Add(run);

            run = new Run();
            run.Text = "g = IGF + FAC";
            newP4.Inlines.Add(run);

            run = new Run();
            run.Text = "\n\r";
            newP4.Inlines.Add(run);

            newP6.TextAlignment = TextAlignment.Left;

            run = new Run();
            run.Text = translations.Get_Translation("lblFormula2");
            newP6.Inlines.Add(run);

            run = new Run();
            run.Text = "\n";
            newP6.Inlines.Add(run);

            run = new Run();
            run.Text = translations.Get_Translation("lblFormula3");
            newP6.Inlines.Add(run);

            run = new Run();
            run.Text = "\n";
            newP6.Inlines.Add(run);

            run = new Run();
            run.Text = translations.Get_Translation("lblFormula4");
            newP6.Inlines.Add(run);

            run = new Run();
            run.Text = "\n";
            newP6.Inlines.Add(run);

            run = new Run();
            run.Text = translations.Get_Translation("lblFormula5");
            newP6.Inlines.Add(run);

            run = new Run();
            run.Text = "\n";
            newP6.Inlines.Add(run);

            run = new Run();
            run.Text = translations.Get_Translation("lblFormula6");
            newP6.Inlines.Add(run);

            run = new Run();
            run.Text = "\n";
            newP6.Inlines.Add(run);

            run = new Run();
            run.Text = translations.Get_Translation("lblFormula7");
            newP6.Inlines.Add(run);

            run = new Run();
            run.Text = "\n\r";
            newP6.Inlines.Add(run);

            newP5.TextAlignment = TextAlignment.Justify;

            run = new Run();
            run.Text = translations.Get_Translation("lblFormula8");
            newP5.Inlines.Add(run);

            run = new Run();
            run.Text = "\n";
            newP5.Inlines.Add(run);

            run = new Run();
            run.Text = translations.Get_Translation("lblFormula9");
            newP5.Inlines.Add(run);

            run = new Run();
            run.Text = "\n\r";
            newP5.Inlines.Add(run);

            run = new Run();
            run.Text = translations.Get_Translation("lblFormula10");
            newP5.Inlines.Add(run);

            run = new Run();
            run.Text = "\n";
            newP5.Inlines.Add(run);

            run = new Run();
            run.Text = translations.Get_Translation("lblFormula9");
            newP5.Inlines.Add(run);

            run = new Run();
            run.Text = translations.Get_Translation("lblFormula11");
            newP5.Inlines.Add(run);

            rtxtFormula.Blocks.Add(newP3);
            rtxtFormula.Blocks.Add(newP4);
            rtxtFormula.Blocks.Add(newP6);
            rtxtFormula.Blocks.Add(newP5);
        }
    }
}
