﻿using Stop_Diabetes.Supernova.Messages;
using Supernova.Entities;
using Supernova.GNow;
using Supernova.Model.Database;
using Supernova.Model.Database.dao;
//using Supernova.Model.Generic;
using System;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace GNow_W10.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Profile : Page
    {
        bool Started = false;
        private LoadTranslations translations;
        private class ItemData
        {
            public int altitude { get; set; }
            public double gravity { get; set; }
            public string info { get; set; }
        }

        //private void AdsControl_ErrorOccurred(object sender, Microsoft.Advertising.WinRT.UI.AdErrorEventArgs e)
        //{
        //    AdsControl.Visibility = Visibility.Collapsed;
        //    AdsControl1.Visibility = Visibility.Visible;
        //}

        private Supernova.Entities.Profile profile;
        public Profile()
        {
            this.InitializeComponent();
            translations = new LoadTranslations();

            //AdsInfo adsInfo = new AdsInfo();
            //AdsControl.Width = adsInfo.GetWidth();
            //AdsControl.Height = adsInfo.GetHeight();
            //AdsControl.ApplicationId = adsInfo.GetApplicationID();
            //AdsControl.AdUnitId = adsInfo.GetAdsUnit();

            try
            {
                var status = Supernova.Core.InAppPurchases.Check_Status("DonationGNow");

                if (status)
                {
                    gridAds.Visibility = Visibility.Collapsed;
                }
            }
            catch { }
        }

        private async void CheckDonation()
        {
            MessageBox msgBox = new MessageBox();

            string answer = "";

            try
            {
                bool result = await Supernova.Core.InAppPurchases.Check_InApp("DonationGNow");
                if (result)
                {
                    answer = translations.Get_Translation("ThankYouInApp");
                    gridAds.Visibility = Visibility.Collapsed;
                }
                else
                {
                    answer = translations.Get_Translation("NoInApp");
                }
            }
            catch (Exception ex) { answer = ex.Message; }
            msgBox.Show(answer);
        }

        private void TextBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {
            CheckDonation();
        }
        private async void GetProfile()
        {
            ProfileDAO pDao = new ProfileDAO();
            profile = await pDao.GetProfile();

            sldGravity.Value = profile.nDecimal;
            sldCalculator.Value = profile.calcDecimal;
            sLocation.IsOn = profile.sync;
            if (profile.units == Units.Meters)
            {
                gravChart.Header = string.Format(translations.Get_Translation("GvsA"), "m/s²", "m");
                cmbUnits.SelectedIndex = 0;
            }
            else
            {
                gravChart.Header = string.Format(translations.Get_Translation("GvsA"), "ft/s²", "ft");
                cmbUnits.SelectedIndex = 1;
            }
        }

        private void cmbUnits_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateProfile();
        }

        private void sldGravity_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            UpdateProfile();
        }

        private void sldCalculator_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            UpdateProfile();
        }

        private void myLocations_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ListView item = (ListView)sender;

            if (item.SelectedItem != null)
            {
                var gravity = item.SelectedItem as GravityData;

                Frame.Navigate(typeof(Map), new Tuple<string, string, string, string, Units>(gravity.altitude.ToString(), gravity.latitude.ToString(), gravity.longitude.ToString(), gravity.gravity.ToString(), profile.units));
            }
        }

        private void UpdateProfile()
        {
            if (Started)
            {
                profile.calcDecimal = (int)sldCalculator.Value;
                profile.nDecimal = (int)sldGravity.Value;
                if (cmbUnits.SelectedIndex == 0)
                    profile.units = Units.Meters;
                else
                    profile.units = Units.Feet;

                profile.sync = sLocation.IsOn;

                ProfileDAO.Update(profile);
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            GetProfile();
            GetGravities();
            Started = true;
        }

        private async void GetGravities()
        {
            Gravity G = null;
            List<ItemData> chartData = new List<ItemData>();
            GravityDataDAO gDao = new GravityDataDAO();
            var gData = await gDao.GetAllData();

            myLocations.ItemsSource = gData;

            foreach (var gDatum in gData)
            {
                G = new Gravity(gDatum.latitude, gDatum.longitude, gDatum.altitude);
                if (profile.units == Units.Meters)
                    chartData.Add(new ItemData() { altitude = gDatum.altitude, gravity = Math.Round(gDatum.gravity, 3), info = string.Format("{0} = {1}\n{2} = {3}", translations.Get_Translation("lblLatitude1\\Text"), gDatum.latitude, translations.Get_Translation("lblLongitude1\\Text"), gDatum.longitude) });
                else
                    chartData.Add(new ItemData() { altitude = gDatum.altitude, gravity = Math.Round(G.ChangeToFeet(gDatum.gravity), 3), info = string.Format("{0} = {1}\n{2} = {3}", translations.Get_Translation("lblLatitude1\\Text"), gDatum.latitude, translations.Get_Translation("lblLongitude1\\Text"), gDatum.longitude) });
            }

            gravChart.DataContext = chartData;
        }

        private void sLocation_Toggled(object sender, RoutedEventArgs e)
        {
            UpdateProfile();
        }
    }
}
