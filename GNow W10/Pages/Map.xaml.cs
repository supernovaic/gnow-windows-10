﻿using Supernova.Core;
using Supernova.Entities;
using Supernova.Model.Database;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace GNow_W10.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Map : Page
    {
        private LoadTranslations translations;
        public Map()
        {
            this.InitializeComponent();
            translations = new LoadTranslations();
        }

        private void btnZoomIn_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (gMap.ZoomLevel < 20)
                gMap.ZoomLevel = gMap.ZoomLevel + 0.5;
        }

        private void btnZoomOut_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (gMap.ZoomLevel > 0)
                gMap.ZoomLevel = gMap.ZoomLevel - 0.5;
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter != null)
            {
                var location = (Tuple<string, string, string, string, Units>)e.Parameter;
                MapCreator mCreator = new MapCreator(gMap);

                var alt = int.Parse(location.Item1);
                var lat = double.Parse(location.Item2);
                var lng = double.Parse(location.Item3);
                var gravity = double.Parse(location.Item4);

                mCreator.SetCenter(lat, lng, 12);

                mCreator.AddPushPin(lat, lng);

                lblAltitude.Text = translations.Get_Translation("lblAltitude2") + " (m):\n" + alt.ToString();
                lblLatitude.Text = translations.Get_Translation("lblLatitude\\PlaceholderText") + ":\n" + Math.Round(lat, 6).ToString();
                lblLongitude.Text = translations.Get_Translation("lblLongitude\\Text") + ":\n" + Math.Round(lng, 6).ToString();
                lblGravity.Text = translations.Get_Translation("lblGravity3") + "\n" + Math.Round(gravity, 6).ToString();
            }
        }
    }
}

